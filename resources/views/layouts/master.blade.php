<!DOCTYPE html>
<html dir="ltr" lang="fr">
<head>

<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="#" />
<meta name="keywords" content="#" />

<title>Cabinet Dentaire Mourad</title>

<!-- Stylesheet -->
<link href="{!! env('APP_URL') !!}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{!! env('APP_URL') !!}/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="{!! env('APP_URL') !!}/css/animate.css" rel="stylesheet" type="text/css">
<link href="{!! env('APP_URL') !!}/css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="{!! env('APP_URL') !!}/css/menuzord-skins/menuzord-border-bottom.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="{!! env('APP_URL') !!}/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="{!! env('APP_URL') !!}/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="{!! env('APP_URL') !!}/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="{!! env('APP_URL') !!}/css/responsive.css" rel="stylesheet" type="text/css">

<!-- CSS | Theme Color -->
<link href="{!! env('APP_URL') !!}/css/colors/theme-skin-blue6.css" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="{!! env('APP_URL') !!}/js/jquery-2.2.4.min.js"></script>
<script src="{!! env('APP_URL') !!}/js/jquery-ui.min.js"></script>
<script src="{!! env('APP_URL') !!}/js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="{!! env('APP_URL') !!}/js/jquery-plugin-collection.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper" class="clearfix">
  <!-- preloader -->
  <div id="preloader">
    <div id="spinner">
      <div class="preloader-dot-loading">
        <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
      </div>
    </div>
    <!-- <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div> -->
  </div>
  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top bg-theme-colored sm-text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
          	<div class="widget no-border m-0">
	          	<ul class="list-inline sm-text-center mt-5">
	          		<li>
	          			<a href="tel:+212622093428" class="text-white"><i class="fa fa-phone"></i> +212-622-093-428</a>
	          		</li>
	          	</ul>
          	</div>
          </div>
          <div class="col-md-4">
            <div class="widget no-border m-0">
              <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">
                <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-facebook text-white"></i></a></li>
                <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-twitter text-white"></i></a></li>
                <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-google-plus text-white"></i></a></li>
                <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-instagram text-white"></i></a></li>
                <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-linkedin text-white"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-white">
        <div class="container">
          <nav id="menuzord-right" class="menuzord blue">
            <a class="menuzord-brand pull-left flip" href="javascript:void(0)">
              <img src="{!! env('APP_URL') !!}/images/logo.png" alt="">
            </a>
            <ul class="menuzord-menu">
              <li class="active"><a href="{!! env('APP_URL') !!}#home">Accueil</a>
              </li>
              <li><a href="{!! env('APP_URL') !!}#">Nos Services</a>
                <ul class="dropdown">
                  <li><a href="{!! env('APP_URL') !!}#">Détartrage</a></li>
                  <li><a href="{!! env('APP_URL') !!}#">Hygiène</a></li>
                </ul>
              </li>
              <li><a href="{!! env('APP_URL') !!}#">Apropos de nous</a></li>
              <li><a href="{!! env('APP_URL') !!}#">Blog</a></li>
              <li><a href="{!! env('APP_URL') !!}#">Nous contacter</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </header>

  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: home -->
    <section id="home" class="divider parallax" data-bg-img="images/bg/bg3.jpg">
      <div class="display-table">
        <div class="display-table-cell">
          <div class="container">
            <div class="row">
              <div class="col-md-4 col-md-offset-8">
                <div class="bg-theme-colored p-25">
                  <h3 class="m-0 mb-30">Prendre un<span class="text-white"> Rendez-vous</span></h3>
                  <!-- Appointment Form -->
                  <form id="home_appointment_form" name="home_appointment_form" method="post" action="includes/appointment.php">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-group mb-10">
                          <input name="form_name" class="form-control" type="text" required="" placeholder="Votre nom complet" aria-required="true">
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group mb-10">
                          <input name="form_email" class="form-control required email" type="email" placeholder="Votre adresse E-mail" aria-required="true">
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group mb-10">
                          <input name="form_appontment_date" class="form-control required datetime-picker" type="text" placeholder="Date et heure du rendez-vous" aria-required="true">
                        </div>
                      </div>
                    </div>
                    <div class="form-group mb-10">
                      <textarea id="form_message" name="form_message" class="form-control required"  placeholder="Votre message (facultatif)" rows="5" aria-required="true"></textarea>
                    </div>
                    <div class="form-group mb-0 mt-10">
                      <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                      <button type="submit" class="btn btn-gray btn-flat" data-loading-text="Veuillez patienter...">Envoyer</button>
                    </div>
                  </form>
    
                  <!-- Appointment Form Validation-->
                  <script type="text/javascript">
                    $("#home_appointment_form").validate({
                      submitHandler: function(form) {
                        var form_btn = $(form).find('button[type="submit"]');
                        var form_result_div = '#form-result';
                        $(form_result_div).remove();
                        form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                        var form_btn_old_msg = form_btn.html();
                        form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                        $(form).ajaxSubmit({
                          dataType:  'json',
                          success: function(data) {
                            if( data.status == 'true' ) {
                              $(form).find('.form-control').val('');
                            }
                            form_btn.prop('disabled', false).html(form_btn_old_msg);
                            $(form_result_div).html(data.message).fadeIn('slow');
                            setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                          }
                        });
                      }
                    });
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: home-boxes -->
    <section>
      <div class="container pt-0 pb-0">
        <div class="section-content">
          <div class="row equal-height-inner home-boxes mt-sm-0" data-margin-top="-100px">
            <div class="col-sm-12 col-md-3 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay1">
              <div class="sm-height-auto bg-theme-colored">
                <div class="p-30">
                  <img src="{!! env('APP_URL') !!}/images/flaticon-png-medical/small/antibiotic.png" width="70" alt="">
                  <h4 class="text-uppercase text-white">Détartrage</h4>
                  <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
                  <a href="{!! env('APP_URL') !!}#" class="btn btn-border btn-circled btn-transparent btn-sm">En savoir plus</a>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-md-3 pl-0 pl-sm-15 pr-0 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay2">
              <div class="sm-height-auto bg-theme-colored-darker2">
                <div class="p-30">
                  <img src="{!! env('APP_URL') !!}/images/flaticon-png-medical/small/spinal-column.png" width="70" alt="">
                  <h4 class="text-uppercase text-white">Hygiène</h4>
                  <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
                  <a href="{!! env('APP_URL') !!}#" class="btn btn-border btn-circled btn-transparent btn-sm">En savoir plus</a>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-md-3 pl-0 pr-0 pl-sm-15 pr-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay3">
              <div class="sm-height-auto bg-theme-colored-darker3">
                <div class="p-30">
                  <img src="{!! env('APP_URL') !!}/images/flaticon-png-medical/small/stethoscope.png" width="70" alt="">
                  <h4 class="text-uppercase text-white">Service 3</h4>
                  <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
                  <a href="{!! env('APP_URL') !!}#" class="btn btn-border btn-circled btn-transparent btn-sm">En savoir plus</a>
                </div>
              </div>
            </div>
            <div class="col-sm-12 col-md-3 pl-0 pl-sm-15 sm-height-auto mt-sm-0 wow fadeInLeft animation-delay4">
              <div class="sm-height-auto bg-theme-colored-darker4">
                <div class="p-30">
                  <img src="{!! env('APP_URL') !!}/images/flaticon-png-medical/small/wheelchair.png" width="70" alt="">
                  <h4 class="text-uppercase text-white">Service 4</h4>
                  <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
                  <a href="{!! env('APP_URL') !!}#" class="btn btn-border btn-circled btn-transparent btn-sm">En savoir plus</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: -->
    <section data-bg-img="images/pattern/p5.png">
      <div class="container pb-70">
        <div class="section-content">
          <div class="row">
            <div class="col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
              <div class="border-10px p-30 bg-white">
                <h5><i class="fa fa-clock-o text-theme-colored"></i> Heures d'ouverture</h5>
                <div class="opening-hours text-left">
                  <ul class="list-unstyled">
                    <li class="clearfix line-height-1"> <span> Lundi - Vendredi </span>
                      <div class="value"> 9.00 - 20.00 </div>
                    </li>
                    <li class="clearfix line-height-1"> <span> Samedi </span>
                      <div class="value"> 10.00 - 16.00 </div>
                    </li>
                    <li class="clearfix line-height-1"> <span> Dimanche </span>
                      <div class="value"> 9.30 - 18.00 </div>
                    </li>
                  </ul>
                </div>
                <h5 class="mt-30"><i class="fa fa-pencil-square-o text-theme-colored"></i> Besoin d'aide?</h5>
                <p class="mt-0">Prenez un rendez-vous pour obtenir de l'aide.</p>
                <a href="{!! env('APP_URL') !!}#" class="btn btn-dark btn-theme-colored btn-sm">Contacter nous maintenant</a>
              </div>
            </div>
            <div class="col-md-8 wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
              <div class="__owl-carousel-1col owl-dots-bottom-right" data-dots="true">
                <div class="item">
                  <div class="row-fluid">
                    <div class="col-md-5">
                      <img src="{!! env('APP_URL') !!}/images/team/h2.jpg" alt="">
                    </div>
                    <div class="col-md-7">
                      <h2 class="line-bottom mt-0">Dr. Mourad Ariba</h2>
                      <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam maxime nesciunt ex modi minus illum nemo provident ducimus, velit magnam consectetur adipisicing nemo provident ducimus, velit magnam.</p>
                      <ul class="styled-icons icon-theme-colored icon-circled icon-dark icon-sm mt-20">
                        <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-skype"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Specialities -->
    <section data-bg-img="images/pattern/p4.png">
      <div class="container pb-70">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase mt-0 line-height-1">Nos Spécialités</h2>
              <div class="title-icon">
                <img class="mb-10" src="{!! env('APP_URL') !!}/images/title-icon.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="section-centent">
          <div class="row">
            <div class="col-md-12">
              <div class="services-tab border-10px bg-white">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="{!! env('APP_URL') !!}#tab11" data-toggle="tab"><i class="flaticon-medical-xray2"></i><span>Spécialité 1</span></a></li>
                  <li><a href="{!! env('APP_URL') !!}#tab12" data-toggle="tab"><i class="flaticon-medical-heart36"></i><span>Spécialité 2</span></a></li>
                  <li><a href="{!! env('APP_URL') !!}#tab13" data-toggle="tab"><i class="flaticon-medical-brain9"></i><span>Spécialités 3</span></a></li>
                  <li><a href="{!! env('APP_URL') !!}#tab14" data-toggle="tab"><i class="flaticon-medical-teeth1"></i><span>Spécialité 4</span></a></li>
                  <li><a href="{!! env('APP_URL') !!}#tab15" data-toggle="tab"><i class="flaticon-medical-hospital35"></i><span>Spécialités 5</span></a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="tab11">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="thumb">
                          <img class="img-fullwidth" src="{!! env('APP_URL') !!}/images/services/1.jpg" alt="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="service-content">
                          <h3 class="sub-title mb-0 mt-15">Services</h3>
                          <h1 class="title mt-0">Spécialité 1</h1>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione.</p>
                          <ul class="mt-10">
                            <li class="mb-10">
                              <h6 class="mt-0 mb-0"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Qualified Doctors</h6>
                              <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo.</small></p>
                            </li>
                            <li class="mb-10">
                              <h6 class="mt-0 mb-0"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;24×7 Emergency Services</h6>
                              <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo.</small></p>
                            </li>
                          </ul>
                          <a class="btn btn-dark btn-theme-colored" href="{!! env('APP_URL') !!}#">En savoir plus</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab12">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="thumb">
                          <img class="img-fullwidth" src="{!! env('APP_URL') !!}/images/services/2.jpg" alt="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="service-content">
                          <h3 class="sub-title mb-0 mt-15">Services</h3>
                          <h1 class="title mt-0">Cardiology</h1>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione.</p>
                          <ul class="mt-10">
                            <li class="mb-10">
                              <h6 class="mt-0 mb-0"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Qualified Doctors</h6>
                              <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo.</small></p>
                            </li>
                            <li class="mb-10">
                              <h6 class="mt-0 mb-0"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;24×7 Emergency Services</h6>
                              <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo.</small></p>
                            </li>
                          </ul>
                          <a class="btn btn-dark btn-theme-colored" href="{!! env('APP_URL') !!}#">View Details</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab13">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="thumb">
                          <img class="img-fullwidth" src="{!! env('APP_URL') !!}/images/services/3.jpg" alt="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="service-content">
                          <h3 class="sub-title mb-0 mt-15">Services</h3>
                          <h1 class="title mt-0">Neurology</h1>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione.</p>
                          <ul class="mt-10">
                            <li class="mb-10">
                              <h6 class="mt-0 mb-0"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Qualified Doctors</h6>
                              <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo.</small></p>
                            </li>
                            <li class="mb-10">
                              <h6 class="mt-0 mb-0"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;24×7 Emergency Services</h6>
                              <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo.</small></p>
                            </li>
                          </ul>
                          <a class="btn btn-dark btn-theme-colored" href="{!! env('APP_URL') !!}#">View Details</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab14">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="thumb">
                          <img class="img-fullwidth" src="{!! env('APP_URL') !!}/images/services/4.jpg" alt="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="service-content">
                          <h3 class="sub-title mb-0 mt-15">Services</h3>
                          <h1 class="title mt-0">Dental</h1>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque.</p>
                          <div class="row mb-20">
                           <div class="col-md-6">
                              <ul class="mt-10">
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Qualified Doctors</li>
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;24×7 Emergency Services</li>
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;General Medical</li>
                              </ul>
                           </div>
                           <div class="col-md-6">
                              <ul class="mt-10">
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Feel like Home Services</li>
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Outdoor Checkup</li>
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Easy and Affordable Billing</li>
                              </ul>
                           </div>
                          </div>
                          <a class="btn btn-dark btn-theme-colored" href="{!! env('APP_URL') !!}#">View Details</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="tab15">
                    <div class="row">
                      <div class="col-md-5">
                        <div class="thumb">
                          <img class="img-fullwidth" src="{!! env('APP_URL') !!}/images/services/5.jpg" alt="">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="service-content">
                          <h3 class="sub-title mb-0 mt-15">Services</h3>
                          <h1 class="title mt-0">Haematology</h1>
                          <p>One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, iste, architecto ullam tenetur quia nemo ratione tempora consectetur quos minus ut quo nulla ipsa aliquid neque.</p>
                          <div class="row mb-20">
                           <div class="col-md-6">
                              <ul class="mt-10">
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Qualified Doctors</li>
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;24×7 Emergency Services</li>
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;General Medical</li>
                              </ul>
                           </div>
                           <div class="col-md-6">
                              <ul class="mt-10">
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Feel like Home Services</li>
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Outdoor Checkup</li>
                                <li class="mb-10"><i class="fa fa-angle-double-right text-theme-colored font-15"></i>&emsp;Easy and Affordable Billing</li>
                              </ul>
                           </div>
                          </div>
                          <a class="btn btn-dark btn-theme-colored" href="{!! env('APP_URL') !!}#">View Details</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Funfact -->
    <section class="divider parallax layer-overlay overlay-white-8" data-bg-img="images/bg/bg2.jpg">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
            <div class="funfact">
              <i class="pe-7s-smile text-black-light mt-20 font-48 pull-left flip"></i>
              <div class="ml-60">
                <h2 class="animate-number text-theme-colored mt-0 mb-10 pb-20 font-48 line-bottom" data-value="754" data-animation-duration="2000">0</h2>
                <div class="clearfix"></div>
                <h4>Patients heureux</h4>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
            <div class="funfact">
              <i class="pe-7s-rocket text-black-light mt-20 font-48 pull-left flip"></i>
              <div class="ml-60">
                <h2 class="animate-number text-theme-colored mt-0 mb-10 pb-20 font-48 line-bottom" data-value="125" data-animation-duration="2500">0</h2>
                <div class="clearfix"></div>
                <h4>Mission réussie</h4>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
            <div class="funfact">
              <i class="pe-7s-add-user text-black-light mt-20 font-48 pull-left flip"></i>
              <div class="ml-60">
                <h2 class="animate-number text-theme-colored mt-0 mb-10 pb-20 font-48 line-bottom" data-value="150" data-animation-duration="3000">0</h2>
                <div class="clearfix"></div>
                <h4>Indice 2</h4>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.6s">
            <div class="funfact">
              <i class="pe-7s-global text-black-light mt-20 font-48 pull-left flip"></i>
              <div class="ml-60">
                <h2 class="animate-number text-theme-colored mt-0 mb-10 pb-20 font-48 line-bottom" data-value="55" data-animation-duration="3500">0</h2>
                <div class="clearfix"></div>
                <h4>Indice 3</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: blog -->
    <section id="blog">
      <div class="container">
        <div class="section-title">
          <div class="row">
            <div class="col-md-6">
              <h2 class="mt-0 text-uppercase font-28">Notre <span class="text-theme-colored font-weight-400">Blog</span> <span class="font-30 text-theme-colored">.</span></h2>
              <div class="icon">
                <i class="fa fa-hospital-o"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
              <article class="post clearfix mb-sm-30 bg-lighter">
                <div class="entry-header">
                  <div class="post-thumb thumb"> 
                    <img src="{!! env('APP_URL') !!}/images/blog/1.jpg" alt="" class="img-responsive img-fullwidth"> 
                  </div>
                </div>
                <div class="entry-content p-20 pr-10">
                  <div class="entry-meta media mt-0 no-bg no-border">
                    <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                      <ul>
                        <li class="font-16 text-white font-weight-600 border-bottom">28</li>
                        <li class="font-12 text-white text-uppercase">Fev</li>
                      </ul>
                    </div>
                    <div class="media-body pl-15">
                      <div class="event-content pull-left flip">
                        <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="{!! env('APP_URL') !!}#">Post title here</a></h4>
                        <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>                       
                        <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>                       
                      </div>
                    </div>
                  </div>
                  <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda. Pariatur iste veritatis excepturi, ipsa optio nobis.</p>
                  <a href="{!! env('APP_URL') !!}#" class="btn-read-more">Lire plus</a>
                  <div class="clearfix"></div>
                </div>
              </article>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
              <article class="post clearfix mb-sm-30 bg-lighter">
                <div class="entry-header">
                  <div class="post-thumb thumb"> 
                    <img src="{!! env('APP_URL') !!}/images/blog/2.jpg" alt="" class="img-responsive img-fullwidth"> 
                  </div>
                </div>
                <div class="entry-content p-20 pr-10">
                  <div class="entry-meta media mt-0 no-bg no-border">
                    <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                      <ul>
                        <li class="font-16 text-white font-weight-600 border-bottom">28</li>
                        <li class="font-12 text-white text-uppercase">Fev</li>
                      </ul>
                    </div>
                    <div class="media-body pl-15">
                      <div class="event-content pull-left flip">
                        <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="{!! env('APP_URL') !!}#">Post title here</a></h4>
                        <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>                       
                        <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>                       
                      </div>
                    </div>
                  </div>
                  <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda. Pariatur iste veritatis excepturi, ipsa optio nobis.</p>
                  <a href="{!! env('APP_URL') !!}#" class="btn-read-more">Lire plus</a>
                  <div class="clearfix"></div>
                </div>
              </article>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
              <article class="post clearfix bg-lighter">
                <div class="entry-header">
                  <div class="post-thumb thumb"> 
                    <img src="{!! env('APP_URL') !!}/images/blog/3.jpg" alt="" class="img-responsive img-fullwidth"> 
                  </div>
                </div>
                <div class="entry-content p-20 pr-10">
                  <div class="entry-meta media mt-0 no-bg no-border">
                    <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                      <ul>
                        <li class="font-16 text-white font-weight-600 border-bottom">28</li>
                        <li class="font-12 text-white text-uppercase">Fev</li>
                      </ul>
                    </div>
                    <div class="media-body pl-15">
                      <div class="event-content pull-left flip">
                        <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a href="{!! env('APP_URL') !!}#">Post title here</a></h4>
                        <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-commenting-o mr-5 text-theme-colored"></i> 214 Comments</span>                       
                        <span class="mb-10 text-gray-darkgray mr-10 font-13"><i class="fa fa-heart-o mr-5 text-theme-colored"></i> 895 Likes</span>                       
                      </div>
                    </div>
                  </div>
                  <p class="mt-10">Lorem ipsum dolor sit amet, consectetur adipisi cing elit. Molestias eius illum libero dolor nobis deleniti, sint assumenda. Pariatur iste veritatis excepturi, ipsa optio nobis.</p>
                  <a href="{!! env('APP_URL') !!}#" class="btn-read-more">Lire plus</a>
                  <div class="clearfix"></div>
                </div>
              </article>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->

  <!-- Footer -->
  <footer id="footer" class="footer" data-bg-img="images/footer-bg.png" data-bg-color="#25272e">
    <div class="container pt-70 pb-40">
      <div class="row">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark"> <img alt="" src="{!! env('APP_URL') !!}/images/logo.png">
            <p class="font-12 mt-10 mb-10">Medinova is a library of corporate and business templates with predefined web elements which helps you to build your own site.</p>
            <a class="btn btn-default btn-transparent btn-xs btn-flat mt-5" href="{!! env('APP_URL') !!}#">En savoir plus</a>
            <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm mt-20">
              <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-skype"></i></a></li>
              <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-youtube"></i></a></li>
              <li><a href="{!! env('APP_URL') !!}#"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom-theme-colored-2">Articles récents</h5>
            <div class="latest-posts">
              <article class="post media-post clearfix pb-0 mb-10">
                <a class="post-thumb" href="{!! env('APP_URL') !!}#"><img src="https://placehold.it/80x55" alt=""></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="{!! env('APP_URL') !!}#">Titre Article 1</a></h5>
                  <p class="post-date mb-0 font-12">Mar 08, 2018</p>
                </div>
              </article>
              <article class="post media-post clearfix pb-0 mb-10">
                <a class="post-thumb" href="{!! env('APP_URL') !!}#"><img src="https://placehold.it/80x55" alt=""></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="{!! env('APP_URL') !!}#">Titre Article 2</a></h5>
                  <p class="post-date mb-0 font-12">Mar 08, 2018</p>
                </div>
              </article>
              <article class="post media-post clearfix pb-0 mb-10">
                <a class="post-thumb" href="{!! env('APP_URL') !!}#"><img src="https://placehold.it/80x55" alt=""></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="{!! env('APP_URL') !!}#">Titire Article 3</a></h5>
                  <p class="post-date mb-0 font-12">Mar 08, 2018</p>
                </div>
              </article>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom-theme-colored-2">Contact Rapide</h5>
            <form id="footer_quick_contact_form" name="footer_quick_contact_form" class="quick-contact-form" action="includes/quickcontact.php" method="post">
              <div class="form-group">
                <input name="form_email" class="form-control" type="text" required="" placeholder="Votre Adresse E-mail">
              </div>
              <div class="form-group">
                <textarea name="form_message" class="form-control" required="" placeholder="Votre message" rows="3"></textarea>
              </div>
              <div class="form-group">
                <input name="form_botcheck" class="form-control" type="hidden" value="" />
                <button type="submit" class="btn btn-default btn-transparent btn-xs btn-flat mt-0" data-loading-text="Veuillez patienter...">Envoyer</button>
              </div>
            </form>

            <!-- Quick Contact Form Validation-->
            <script type="text/javascript">
              $("#footer_quick_contact_form").validate({
                submitHandler: function(form) {
                  var form_btn = $(form).find('button[type="submit"]');
                  var form_result_div = '#form-result';
                  $(form_result_div).remove();
                  form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                  var form_btn_old_msg = form_btn.html();
                  form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                  $(form).ajaxSubmit({
                    dataType:  'json',
                    success: function(data) {
                      if( data.status == 'true' ) {
                        $(form).find('.form-control').val('');
                      }
                      form_btn.prop('disabled', false).html(form_btn_old_msg);
                      $(form_result_div).html(data.message).fadeIn('slow');
                      setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                    }
                  });
                }
              });
            </script>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom-theme-colored-2">Opening Hours</h5>
            <div class="opening-hours">
              <ul class="list list-border">
                <li class="clearfix"> <span><i class="fa fa-clock-o mr-5"></i> Mardi - Vendredi :</span>
                  <div class="value pull-right"> 9.00 - 20.00 </div>
                </li>
                <li class="clearfix"> <span class="text-theme-color-2"><i class="fa fa-clock-o mr-5"></i> Samedi </span>
                  <div class="value pull-right text-white"> 10.00 - 16.00 </div>
                </li>
                <li class="clearfix"> <span><i class="fa fa-clock-o mr-5"></i> Dimanche :</span>
                  <div class="value pull-right text-white"> 9.30 - 18.00 </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="{!! env('APP_URL') !!}#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="{!! env('APP_URL') !!}/js/custom.js"></script>

</body>
</html>